import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SummaryExpensesComponent } from '../app/pages/summary-expenses/summary-expenses.component'
import { DetailedExpensesComponent } from '../app/pages/detailed-expenses/detailed-expenses.component'

const routes: Routes = [
  {
    path: "summary-expenses",
    component: SummaryExpensesComponent
  },
  {
    path: "detailed-expenses",
    component: DetailedExpensesComponent
  },
  {
    path: "",
    redirectTo: "/summary-expenses",
    pathMatch: 'full'
  },
  {
    path: "**",
    redirectTo: "/summary-expenses",
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

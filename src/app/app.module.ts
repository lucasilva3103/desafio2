import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material-module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SummaryExpensesComponent } from './pages/summary-expenses/summary-expenses.component';
import { DetailedExpensesComponent } from './pages/detailed-expenses/detailed-expenses.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './common/header/header.component';
import { ExpenseTableComponent } from './common/expense-table/expense-table.component';

@NgModule({
  declarations: [
    AppComponent,
    SummaryExpensesComponent,
    DetailedExpensesComponent,
    HeaderComponent,
    ExpenseTableComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-expense-table',
  templateUrl: './expense-table.component.html',
  styleUrls: ['./expense-table.component.css']
})
export class ExpenseTableComponent implements OnInit {

  @Input()
  tableHeaders: any;

  @Input()
  tableData: any;

  tableType: any;

  constructor() { }

  ngOnInit(): void {
    switch (this.tableHeaders) {
      case 'summary':
        this.tableType = 'summary';
        this.tableHeaders = ['Mês', 'Total Gasto'];
        break;
      case 'detailed':
        this.tableType = 'detailed'
        this.tableHeaders = ['Origem', 'Categoria', 'Valor Gasto', 'Mês']
        break;
      default:
        this.tableHeaders = []
    }
  }


  formatNumberInBrlCurrency(number) {
    return number.toLocaleString('pt-br', { style: 'currency', currency: 'BRL' })
  };

  getMonthLabel(month) {
    switch (month) {
      case 1:
        return "Janeiro"
      case 2:
        return "Fevereiro"
      case 3:
        return "Março"
      case 4:
        return "Abril"
      case 5:
        return "Maio"
      case 6:
        return "Junho"
      case 7:
        return "Julho"
      case 8:
        return "Agosto"
      case 9:
        return "Setembro"
      case 10:
        return "Outubro"
      case 11:
        return "Novembro"
      case 12:
        return "Dezembro"
    };
  };

  getCategoryLabeL(category) {
    switch (category) {
      case 1:
        return "Transporte"
      case 2:
        return "Games"
      case 3:
        return "Higiene"
      case 4:
        return "Mecanica"
      case 5:
        return "Restaurantes"
      case 6:
        return "Mercados"
    }
  }

}

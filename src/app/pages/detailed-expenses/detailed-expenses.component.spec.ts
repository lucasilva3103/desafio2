import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailedExpensesComponent } from './detailed-expenses.component';

describe('DetailedExpensesComponent', () => {
  let component: DetailedExpensesComponent;
  let fixture: ComponentFixture<DetailedExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailedExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ExpensesServiceService } from 'src/app/services/expenses-service.service';

@Component({
  selector: 'app-detailed-expenses',
  templateUrl: './detailed-expenses.component.html',
  styleUrls: ['./detailed-expenses.component.css']
})
export class DetailedExpensesComponent implements OnInit {

  allExpenses: any;
  summaryExpenses: any;
  detailedExpenses: any;
  showLoader: boolean = true;

  constructor(private expenseService: ExpensesServiceService) { }

  ngOnInit(): void {
    this.getExpenseData();
  }

  async getExpenseData() {
    let storagedExpenses = JSON.parse(sessionStorage.getItem('expenses'));

    if ('expenses' in sessionStorage) {
      this.allExpenses = storagedExpenses;
    } else {
      this.allExpenses = await this.expenseService.getExpenses();
    }
    this.loadDetailedTable(this.allExpenses);
  };

  loadDetailedTable(expenses) {
    this.showLoader = false;
    this.detailedExpenses = expenses.sort((a, b) => a.mes_lancamento - b.mes_lancamento);
  };


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SummaryExpensesComponent } from './summary-expenses.component';

describe('SummaryExpensesComponent', () => {
  let component: SummaryExpensesComponent;
  let fixture: ComponentFixture<SummaryExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SummaryExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ExpensesServiceService } from '../../services/expenses-service.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-summary-expenses',
  templateUrl: './summary-expenses.component.html',
  styleUrls: ['./summary-expenses.component.css']
})
export class SummaryExpensesComponent implements OnInit {

  allExpenses: any;
  summaryExpenses: any;
  detailedExpenses: any;
  showLoader: boolean = true;

  constructor(private expenseService: ExpensesServiceService, private router: Router) { }

  ngOnInit(): void {
    this.getExpenseData();
  }

  async getExpenseData() {
    let storagedExpenses = JSON.parse(sessionStorage.getItem('expenses'));

    if ("expenses" in sessionStorage) {
      this.allExpenses = storagedExpenses;
    } else {
      this.allExpenses = await this.expenseService.getExpenses();
    }
    this.loadSummaryTable(this.allExpenses);
  };

  loadSummaryTable(expenses) {
    this.showLoader = false;

    this.summaryExpenses = expenses.reduce((acumulador, cur) => {
      if (!cur.valor) return acumulador;

      const foundMonths = acumulador.find(e => e.mes_lancamento === cur.mes_lancamento);

      if (foundMonths) {
        foundMonths.valor += cur.valor
      } else {
        acumulador.push({ mes_lancamento: cur.mes_lancamento, valor: cur.valor })
      }

      return acumulador
    }, []).sort((a, b) => a.mes_lancamento - b.mes_lancamento);

    return this.summaryExpenses
  };

}

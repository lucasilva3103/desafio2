import { Injectable } from '@angular/core';
import axios from "axios";
import { environment } from "src/environments/environment";
import { Expenses } from '../models/expenses-model'


@Injectable({
  providedIn: 'root'
})
export class ExpensesServiceService {

  constructor() { }

  async getExpenses(): Promise<Array<Expenses>> {
    const response = await axios.get(`${environment.baseUrl}/lancamentos`);
    sessionStorage.setItem('expenses', JSON.stringify(response.data));
    return response.data
  }

}
